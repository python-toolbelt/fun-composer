# fun-composer
[![pipeline status](https://gitlab.com/python-toolbelt/fun-composer/badges/main/pipeline.svg)](https://gitlab.com/python-toolbelt/fun-composer/-/commits/main)
[![coverage report](https://gitlab.com/python-toolbelt/fun-composer/badges/main/coverage.svg)](https://gitlab.com/python-toolbelt/fun-composer/-/commits/main)

*Is a small library that allows you to compose functions into a functional pipeline using left-to-right function composition.*

**This is the readme for developers.** The documentation for users is available here: [https://python-toolbelt.gitlab.io/fun-composer](https://python-toolbelt.gitlab.io/fun-composer)

## Want to contribute?

Contributions are welcome! Simply fork this project on gitlab, commit your contributions, and create merge requests.

Here is a non-exhaustive list of interesting open topics: [https://gitlab.com/python-toolbelt/fun-composer/-/issues](https://gitlab.com/python-toolbelt/fun-composer/-/issues)

## Requirements for builds

Install requirements for setup beforehand using

```bash
poetry install -E test -E build
```

## Running the tests

This project uses `pytest`.

```bash
pytest
```

## License

This project is licensed under the terms of the MIT license. See [LICENSE](https://gitlab.com/python-toolbelt/fun-composer/-/blob/main/LICENSE) for more information.
