# fun-composer

*Is a small library that allows you to compose functions into a functional pipeline using left-to-right function composition.*

The approach came from Functional Programming, where simple functions are composed into more complex functions in order to transform some data. `fun-composer` provides a DSL to define and compose these transformations.

For documentation see tests: [https://gitlab.com/python-toolbelt/fun-composer/-/tree/main/tests](https://gitlab.com/python-toolbelt/fun-composer/-/tree/main/tests)

## Want to contribute?

Contributions are welcome! Simply fork this project on gitlab, commit your contributions, and create merge requests.

Here is a non-exhaustive list of interesting open topics: [https://gitlab.com/python-toolbelt/fun-composer/-/issues](https://gitlab.com/python-toolbelt/fun-composer/-/issues)

## Requirements

Install requirements for setup beforehand using

```bash
poetry install -E test
```

## Running the tests

This project uses `pytest`.

```bash
pytest
```

## See Also

This library was inspired by:

* Ruby's library called [transproc](https://github.com/solnic/transproc)
* Python's one named [compose](https://pypi.org/project/compose/)

## License

This project is licensed under the terms of the MIT license. See [LICENSE](https://gitlab.com/python-toolbelt/fun-composer/-/blob/main/LICENSE) for more information.
