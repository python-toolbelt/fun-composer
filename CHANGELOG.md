# Changelog

Tracking changes in `fun-composer` between versions. For a complete view of all the releases, visit GitLab:

https://gitlab.com/python-toolbelt/fun-composer/-/releases

## next release
